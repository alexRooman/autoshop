<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cPage extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mConfig');
		$this->load->model('mPage');
	}

	public function index() {
		$this->show('home');
	}	

	public function show($page) {

		// Load page data
		$this->mPage->load($page);

		if (!$this->mPage->data->is_active) {
			show_404();
		}

		// Check the meta-description of the page
		if (!$this->mPage->data->meta_description) {
			// If there is no meta-description, load the default
			$this->mPage->data->meta_description = $this->mConfig->load('default_description')->get();
		}

		// Check the meta-keywords of the page.
		if (!$this->mPage->data->meta_keywords) {
			// If there is no meta-keywords, load the default
			$this->mPage->data->meta_keywords = $this->mConfig->load('default_keywords')->get();
		}

		// Make the title
		$this->mPage->data->title .= ' | ' . $this->mConfig->load('default_title')->get();

		// Collect all data into array to push into view
		$_data = array(
			'url'         => $this->mPage->data->url,
			'description' => $this->mPage->data->meta_description,
			'keywords'    => $this->mPage->data->meta_keywords,
			'title'       => $this->mPage->data->title,
			'header'      => $this->mPage->data->header,
			'content'     => $this->mPage->data->content

		);

		// Render the template
		$this->load->view('pages/' . $this->mPage->data->template . '.php', $_data);

	}

}