<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo $title; ?></title>
	<?php if (htmlspecialchars(trim($description))): ?>
		<meta name="description" content="<?php echo htmlspecialchars(trim($description)); ?>">
	<?php endif; ?>
	<?php if (htmlspecialchars(trim($keywords))): ?>
		<meta name="description" content="<?php echo htmlspecialchars(trim($keywords)); ?>">
	<?php endif; ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
</head>
<body class="cms-page page-<?php echo $url; ?>">
<div class="container">
	<nav class="navbar navbar-toggleable-md navbar-inverse bg-primary">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="#"><?php echo $this->mConfig->load('site_name')->get(); ?></a>
	</nav>
</div>